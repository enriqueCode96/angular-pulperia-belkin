import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  // ruta inicial
  {
    path: '',
    component: HomeComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  // exporto el router
  exports: [
    RouterModule
  ]
})

// clase
export class HomeRoutingModule {

}
