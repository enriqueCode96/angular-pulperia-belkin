import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  // creo el array de imagenes
  images: string[] = [
    '../../assets/images/banner_1.png',
    '../../assets/images/banner_2.png',
    '../../assets/images/banner_3.png'
  ];

  constructor() { }



  ngOnInit(): void {
  }

}
