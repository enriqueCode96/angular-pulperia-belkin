import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent {

  title = 'pulperiaBelkin';

  // array de datos
  items = ['enrique', 'diana', 'garcia'];

  power = 10;

  ngonInit(): void{

  }
  // array de productos
  // para tipar el array de prodcutos



  // metodo del click para agregar un item
  addItem(): void{
    // agrego un item al array (items)

    this.items.push('nuevo item');
  }

  // borra los items de la lista, metodo
  deleteItem(index: number): void{
    this.items.splice(index, 1);
  }

}
