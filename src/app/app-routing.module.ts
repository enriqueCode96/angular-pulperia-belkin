import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';


import {ProductComponent} from './product/components/product/product.component';
import { ContactComponent } from './contact/contact.component';
import {DemoComponent} from './demo/demo.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductDetailComponent } from './product/components/product-detail/product-detail.component';
import { LayoutComponent} from './layout/layout.component';

// importo el guardian
import { AdminGuard } from './admin.guard';


const routes: Routes = [

  // redireciono a la pagina home si el usuario marca nada en la url
  {
    path: '',
    component: LayoutComponent,
    // se vasan del layout
    children: [
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      },
      // creo la rutas que son objetos
      {
      // la ruta en el url
      path: 'home',
      // enlazo el componente para que atienda el HOME
      // component: HomeComponent
      // carga un modulo dinamicamente
      loadChildren: () => import ('./home/home.module').then(m => m.HomeModule) // se resulve una importacion
    },
    {
      path: 'products',
      // component: ProductsComponent
      canActivate: [AdminGuard],
      loadChildren: () => import ('./product/product.module').then(m => m.ProductModule)
    },
    {
      path: 'products/:id', // recibe un parametro dinamico
      component: ProductDetailComponent
      // loadChildren: () => import ( './products/products.component' ).then(m => m.ProductsComponent)
    },
    {
      path: 'contact',
      canActivate: [AdminGuard], // le mando como un array el guardian
      // component: ContactComponent
      loadChildren: () => import ('./contact/contact.component').then(m => m.ContactComponent)
    },
    {
      path: 'order',
      // component: ContactComponent
      loadChildren: () => import ('./order/order.module').then(m => m.OrderModule)
    },
    {
      path: 'demo',
      // component: DemoComponent
      canActivate: [AdminGuard],
      loadChildren: () => import ('./demo/demo.component').then(m => m.DemoComponent)
    }
    ]
  },
  {
    path: 'admin',
    loadChildren: () => import ('./admin/admin.module').then(m => m.AdminModule)
  },
    {
      path: '**',
      component: PageNotFoundComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {

    preloadingStrategy: PreloadAllModules // estrategia de precarga
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
