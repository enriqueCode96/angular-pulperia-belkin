import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators  } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductsService } from '../../../core/services/products/products.service';

@Component({
  selector: 'app-form-produ',
  templateUrl: './form-produ.component.html',
  styleUrls: ['./form-produ.component.scss']
})
export class FormProduComponent implements OnInit {

  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private productsService: ProductsService, private router:Router) {
    this.buildForm();
   }

  ngOnInit(): void {
  }

  // metodo de guardar producto
  saveProduct(event: Event){
    event.preventDefault();

    // pregunto si el formulario viene valido para insertar los datos
    if (this.form.valid) {
        const product = this.form.value;
        this.productsService.craeteProduct(product).subscribe((newProduct) => {
          console.log(newProduct);
          this.router.navigate(['./admin/products'])
        });
    }
    //console.log(this.form.value);
  }


  private buildForm(){
    this.form = this.formBuilder.group({
      id: ['', [Validators.required]],
      title: ['', [Validators.required]],
      price: ['', [Validators.required]],
      image: [''],
      description: ['', [Validators.required]]
    });
  }


}
