import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProduComponent } from './form-produ.component';

describe('FormProduComponent', () => {
  let component: FormProduComponent;
  let fixture: ComponentFixture<FormProduComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormProduComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProduComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
