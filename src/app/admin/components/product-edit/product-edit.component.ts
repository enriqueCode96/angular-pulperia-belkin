import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators  } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductsService } from '../../../core/services/products/products.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

  form: FormGroup;
  id: string;


  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private router:Router,
    private activatedRoute: ActivatedRoute
    )
     {this.buildForm();}

  ngOnInit(): void {
    // para saber el ID del producto
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params.id;

      this.productsService.getProducts(this.id).subscribe(product =>{

        // le inserto la informacion al formulario
        this.form.patchValue(product)
      });
    })
  }

  // metodo de guardar producto
  saveProduct(event: Event){
    event.preventDefault();

    // pregunto si el formulario viene valido para insertar los datos
    if (this.form.valid) {
        const product = this.form.value;
        this.productsService.updateProduct(this.id, product).subscribe((newProduct) => {
          console.log(newProduct);
          this.router.navigate(['./admin/products'])
        });
    }
    //console.log(this.form.value);
  }


  private buildForm(){
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      price: ['', [Validators.required]],
      image: [''],
      description: ['', [Validators.required]]
    });
  }


}
