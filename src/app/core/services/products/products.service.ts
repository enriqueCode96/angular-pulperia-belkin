import { Injectable } from '@angular/core';

// modulo ciente para hacer peticiones
import { HttpClient } from '@angular/common/http';

import { Product } from './../../../product.model';
import { ProductComponent } from 'src/app/product/components/product/product.component';
import { environment } from './../../../../environments/environment';
import { RouteReuseStrategy } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class ProductsService {

  constructor(
    // inyecto la dependencia
    private http: HttpClient
  ) { }

  //  metodo  que devuelve todos los productos
  getAllProducts() {
    return this.http.get<Product[]>(`${environment.url_api}/products`);
  }

  getProducts(id: string ) {
    return this.http.get<Product>(`${environment.url_api}/products/${id}`);
  }

  // metodo para crear un producto
  craeteProduct(product: Product){
    return this.http.post(`${environment.url_api}/products`, product);
  }

  updateProduct(id: string, changes: Partial<Product>){ // partial es que le puedes mandar una cierta parte del cambio opcional
    return this.http.put(`${environment.url_api}/products/${id}`, changes);
  }

   // metodo de  borrar
   deleteProduct(id: string){
    return this.http.delete(`${environment.url_api}/products/${id}`);
  }

}
