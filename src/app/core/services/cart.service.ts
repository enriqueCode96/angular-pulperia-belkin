import { Injectable } from '@angular/core';

// libreria para añadir principios reactivos
import { BehaviorSubject } from 'rxjs';


// modelo de products
import { Product } from '../../product.model';


@Injectable({
  providedIn: 'root'
})


export class CartService {
   private products: Product[] = [];
   private cart = new BehaviorSubject<Product[]>([]);// carrito inicia con 0 productos

   cart$ = this.cart.asObservable();

  constructor() { }


  addCart(product: Product){
    this.products = [...this.products, product];
    this.cart.next(this.products);
  }
}
