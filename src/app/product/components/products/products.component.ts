import { Component, OnInit } from '@angular/core';

// importo el modelo de productos
import { Product } from '../../../product.model';

import { ProductsService } from './../../../core/services/products/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];

  constructor(
    private productsService: ProductsService
  ) { }

  ngOnInit(): void {
    // llamo a mi metodo que se encarga de traer los productos
    this.fetchProducts();
  }

  // metodo para emitir el id
  clickProduct(id: number): void{
    console.log('product');
    console.log(id);
  }

  // metodo para traer los productos
  fetchProducts(): void {
    this.productsService.getAllProducts().subscribe(ArrayDeproducts => {
      // console.log(ArrayDeproducts);

      // renderizo
      this.products =  ArrayDeproducts;
    });
  }

}
