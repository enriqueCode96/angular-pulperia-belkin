import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Params } from '@angular/router';
import { ProductsService } from '../../../core/services/products/products.service';
import { Product } from '../../../product.model';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: Product;

  constructor(
    // inyenccion de dependencia
    private route: ActivatedRoute,
    private productsService: ProductsService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      // console.log(params);
      const id = params.id;
      this.fetchProduct(id);
      // console.log(id);
      // this.product  =  this.productsService.getProducts(id);
      // console.log(product);
    });
  }
  // metodo
  fetchProduct(id: string) {
    this.productsService.getProducts(id).subscribe(product => {
      this.product = product;
      // console.log(product);
    });
  }

  createProduct(){

    // creo un nuevo producto
    const newProduct: Product = {
    id: '222',
    title:  'nuevo desde angular',
    image: 'assets/images/banner_1.png',
    price: 300,
    description: 'nuevo producto'

    };
    // llamo a mi servicio
    this.productsService.craeteProduct(newProduct).subscribe(product => {
      console.log(product)
      // console.log(product);
    });
  }

  // metodo de actualizar
  updateProduct(){
    const updateProduct: Partial<Product> = {
      price: 6969,
      description: 'edicion titulo'
      };

    this.productsService.updateProduct('1', updateProduct).subscribe(product => {
      console.log(product);
      // console.log(product);
    });
  }

  // metodo de borrar
  deleteProduct(){
    this.productsService.deleteProduct('1').subscribe(respuesta => {
      console.log(respuesta);
      // console.log(product);
    });
  }


}
