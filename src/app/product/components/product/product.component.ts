// decoradores para saber que  rol va a cumplir
import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  OnInit,
  DoCheck,
  OnDestroy
} from '@angular/core';

import { Product} from '../../../product.model';

import { CartService } from './../../../core/services/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html', // la ruta
  styleUrls: ['./product.component.scss']

})

// defino la clase de producto
export class ProductComponent  implements  OnInit, DoCheck, OnDestroy{ // implementa la interfaz de Onchanges y OnInit

  // va a recibir propiedades
  @Input() product: Product ;
  @Output() productClicked: EventEmitter<any> = new EventEmitter(); // valor  por defecto

  // objeto tipo fecha
  today = new Date();

  // constructor
  constructor(private cartService: CartService){
    console.log('1. constructor');
  }

  // ngOnChanges, detecta los cambios, actual y anterior
  // ngOnChanges(changes: SimpleChanges){
    //  console.log('2. ngOnchanges');
    // console.log(changes);
  // }

  ngOnInit(): void{
    //console.log('3. ngOnInit');

  }

  // detencion de cambios a mi manera
  ngDoCheck(): void{
    //console.log('4. DoCheck');
  }

  // para remover
  ngOnDestroy(): void{
    console.log('5. OnDestroy');
  }

  // metodo
  addCart(): void{
    console.log('añadir al carrito');
    this.cartService.addCart(this.product);
    //this.productClicked.emit(this.product.id); // emito el valor que quiero
  }

}
