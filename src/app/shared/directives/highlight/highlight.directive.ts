// se crea la directiva con ng g d highlight
import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})

export class HighlightDirective {

  constructor(
    element: ElementRef
   ) {
     // modifico el elemento nativo del html
     element.nativeElement.style.backgroundColor = 'red';
   }

}
